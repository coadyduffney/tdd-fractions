package com.coadyduffney;

/**
 *
 * @author Coady Duffney
 */
public class NumberTheory {

    public static int gcd(int a, int b) {
        while (b != 0) {
            int t = b;
            b = a % t;
            a = t;
        }
        return Math.abs(a);
    }
    
    // function to calculate LCM of two numbers
    // https://www.geeksforgeeks.org/least-common-denominator-lcd/
    public static int lcm(int a, int b) {
        return (a * b) / gcd(a, b);
    }
    
}
