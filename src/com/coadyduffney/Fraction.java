package com.coadyduffney;

/**
 *
 * @author Coady Duffney
 */
public class Fraction {

    private int denominator;
    private int numerator;

    public Fraction(int integerValue) {
        this(integerValue, 1);
    }

    public Fraction(int numerator, int denominator) {
        final int signOfDenominator = denominator < 0 ? -1 : 1;
        final int gcd = NumberTheory.gcd(numerator, denominator) * signOfDenominator;

        this.numerator = numerator / gcd;
        this.denominator = denominator / gcd;
    }

    public Fraction plus(Fraction that) {
        return new Fraction(this.numerator * that.denominator + that.numerator * this.denominator,
                this.denominator * that.denominator);
    }

    public Fraction subtract(Fraction that) {
        int lcd = NumberTheory.lcm(this.denominator, that.denominator);

        if (this.denominator != that.denominator) {
            this.numerator *= (lcd / this.denominator);
            that.numerator *= (lcd / that.denominator);
        }

        return new Fraction(this.numerator - that.numerator, lcd);
    }

    public Fraction multiply(Fraction that) {
        return new Fraction(this.numerator * that.numerator, this.denominator * that.denominator);
    }

    public Fraction divide(Fraction that) {

        if (that.numerator == 0 || that.denominator == 0) {
            return null;
        }

        if (this.numerator == 0) {
            return new Fraction(0);
        }

        if (this.denominator == 1 && that.denominator == 1) {
            return new Fraction(this.numerator / that.numerator);
        }

        Fraction recip = new Fraction(that.denominator, that.numerator);
        return new Fraction(this.numerator * recip.numerator, this.denominator * recip.denominator);
    }

    @Override
    public String toString() {
        return String.format("%d/%d", numerator, denominator);
    }

    @Override
    public int hashCode() {
        return numerator * 19 + denominator;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Fraction) {
            Fraction that = (Fraction) other;
            return this.numerator == that.numerator
                    && this.denominator == that.denominator;
        }
        return false;
    }
}
