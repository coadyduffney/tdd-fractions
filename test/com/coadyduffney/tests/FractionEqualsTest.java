/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coadyduffney.tests;

import com.coadyduffney.Fraction;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Coady Duffney
 */
public class FractionEqualsTest {
    
    public FractionEqualsTest() {
    }
    
    @Test
    public void sameNumeratorAndDenominator() {        
        assertEquals(new Fraction(3,5), new Fraction(3,5));
    }
    
    @Test
    public void differentNumerators() {
        assertNotEquals(new Fraction(1,5), new Fraction(2,5));
    }
    
    @Test
    public void differentDenominators() {
        assertNotEquals(new Fraction(3,4), new Fraction(3,7));
    }
    
    @Test
    public void wholeNumbersEqualsSameFraction() {
        assertEquals(new Fraction(5,1), new Fraction(5));
    }
    
    @Test
    public void wholeNumberNotEqualToDifferentWholeNumber() {
        assertNotEquals(new Fraction(6), new Fraction(5));
    }
    
    @Test
    public void negativeDenominator() {
        assertEquals(new Fraction(1,2), new Fraction(-1,-2));
        assertEquals(new Fraction(-1,2), new Fraction(1,-2));
    }
}
