/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coadyduffney.tests;

import com.coadyduffney.Fraction;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Coady Duffney
 */
public class MultiplyFractionsTest {

    public MultiplyFractionsTest() {
    }
    
    @Test
    public void zeroMultiplyZero() {
        assertEquals(new Fraction(0), new Fraction(0).multiply(new Fraction(0)));
    }    
    
    @Test
    public void wholeNumberMultiplyZero() {
        assertEquals(new Fraction(0), new Fraction(5).multiply(new Fraction(0)));
    }
    
    @Test
    public void positiveWholeNumbers() {
        assertEquals(new Fraction(18), new Fraction(3).multiply(new Fraction(6)));
    }
    
    @Test
    public void wholeNumberMultiplyOne() {
        assertEquals(new Fraction(10), new Fraction(10).multiply(new Fraction(1)));
        assertEquals(new Fraction(23), new Fraction(23).multiply(new Fraction(1)));
    }
    
    @Test
    public void negativeWholeNumbers() {
        assertEquals(new Fraction(35), new Fraction(-5).multiply(new Fraction(-7)));
        assertEquals(new Fraction(-35), new Fraction(-5).multiply(new Fraction(7)));
    }
    
    @Test
    public void differentNumeratorsAndDenominators() {
        assertEquals(new Fraction(5,24), new Fraction(5,8).multiply(new Fraction(3,9)));
    }
    
    @Test
    public void sameDenominators() {
        assertEquals(new Fraction(1,4), new Fraction(1,2).multiply(new Fraction(1,2)));
        assertEquals(new Fraction(4,25), new Fraction(2,5).multiply(new Fraction(2,5)));
    }
    
    @Test
    public void negativeDenominators() {
        assertEquals(new Fraction(4,25), new Fraction(2,-5).multiply(new Fraction(2,-5)));
        assertEquals(new Fraction(-4,25), new Fraction(2,5).multiply(new Fraction(2,-5)));
    }
}
