/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coadyduffney.tests;

import com.coadyduffney.Fraction;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Coady Duffney
 */
public class SubtractFractionsTest {
    
    public SubtractFractionsTest() {
    }
    
    @Test
    public void zeroSubtractZero() {
        assertEquals(new Fraction(0), new Fraction(0).subtract(new Fraction(0)));
    }
    
    @Test
    public void nonZeroSubtractZero() {
        assertEquals(new Fraction(5), new Fraction(5).subtract(new Fraction(0)));
    }
    
    @Test
    public void zeroSubtractNonZero() {
        assertEquals(new Fraction(-5), new Fraction(0).subtract(new Fraction(5)));
    }
    
    @Test
    public void nonZeroSubtractNonZero() {
        assertEquals(new Fraction(5), new Fraction(10).subtract(new Fraction(5)));
    }    
    
    @Test
    public void negativeNonZeroSubtractNonZero() {
        assertEquals(new Fraction(-4), new Fraction(-3).subtract(new Fraction(1)));
        assertEquals(new Fraction(-15), new Fraction(-10).subtract(new Fraction(5)));
    }   
    
    @Test
    public void nonZeroSubtractNegativeNonZero() {
        assertEquals(new Fraction(7), new Fraction(5).subtract(new Fraction(-2)));
        assertEquals(new Fraction(20), new Fraction(15).subtract(new Fraction(-5)));
    }
    
    @Test
    public void commonDenominators() {
        assertEquals(new Fraction(2,5), new Fraction(4,5).subtract(new Fraction(2,5)));
        assertEquals(new Fraction(-1,1), new Fraction(-2,3).subtract(new Fraction(1,3)));
    }
    
    @Test
    public void differentDenominators() {
        assertEquals(new Fraction(3,24), new Fraction(5,8).subtract(new Fraction(3,6)));
        assertEquals(new Fraction(1,36), new Fraction(3,12).subtract(new Fraction(2,9)));
    }
    
    @Test
    public void differentDenominatorsWithNegatives() {
        assertEquals(new Fraction(-9,8), new Fraction(-5,8).subtract(new Fraction(3,6)));
    }
    
    @Test
    public void differentSigns() {
        assertEquals(new Fraction(-7,6), new Fraction(-5,6).subtract(new Fraction(2, 6)));
    }
}
