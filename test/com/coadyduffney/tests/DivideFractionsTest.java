/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coadyduffney.tests;

import com.coadyduffney.Fraction;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Coady Duffney
 */
public class DivideFractionsTest {
    
    public DivideFractionsTest() {
    }
    
    @Test
    public void zeroDivideZero() {
        assertEquals(null, new Fraction(0).divide(new Fraction(0)));
    }
    
    @Test
    public void nonZeroDivideZero() {
        assertEquals(null, new Fraction(8).divide(new Fraction(0)));
    }
    
    @Test
    public void zeroDivideNonZero() {
        assertEquals(new Fraction(0), new Fraction(0).divide(new Fraction(5)));
    }
    
    @Test
    public void wholeNumbersNotZero() {
        assertEquals(new Fraction(5), new Fraction(10).divide(new Fraction(2)));
        assertEquals(new Fraction(10), new Fraction(100).divide(new Fraction(10)));
    }
    
    @Test
    public void wholeNumbersWithNegatives() {
        assertEquals(new Fraction(-10), new Fraction(100).divide(new Fraction(-10)));
        assertEquals(new Fraction(10), new Fraction(-100).divide(new Fraction(-10)));
    }
    
    @Test
    public void nonCommonDenominators() {
        assertEquals(new Fraction(6,5), new Fraction(3,5).divide(new Fraction(1,2)));
        assertEquals(new Fraction(6, 35), new Fraction(2,5).divide(new Fraction(7,3)));
        assertEquals(new Fraction(8,18), new Fraction(4,6).divide(new Fraction(3,2)));
    }
    
    @Test
    public void commonDenominators() {
        assertEquals(new Fraction(50,30), new Fraction(5,10).divide(new Fraction(3,10)));
    }
    
    @Test
    public void divideFractionWithZero() {
        assertEquals(null, new Fraction(5,3).divide(new Fraction(3,0)));
        assertEquals(null, new Fraction(5,10).divide(new Fraction(0,5)));
        
        assertEquals(new Fraction(0,1), new Fraction(0,5).divide(new Fraction(3,10)));
    }
}
