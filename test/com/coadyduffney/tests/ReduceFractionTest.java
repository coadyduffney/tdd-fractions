/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coadyduffney.tests;

import com.coadyduffney.Fraction;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Coady Duffney
 */
public class ReduceFractionTest {
    
    public ReduceFractionTest() {
    }
    
    @Test
    public void alreadyInLowestTerms() {        
        assertEquals(new Fraction(3,4), new Fraction(3,4));
    }
    
    @Test
    public void reduceToNotWholeNumber() {
        assertEquals(new Fraction(3,4), new Fraction(6,8));
    }
    
    @Test
    public void reduceToWholeNumber() {
        assertEquals(new Fraction(6), new Fraction(24, 4));
    }
    
    @Test
    public void reduceZero() {
        assertEquals(new Fraction(0), new Fraction(0, 172635));
    }

}
