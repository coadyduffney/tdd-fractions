/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coadyduffney.tests;

import com.coadyduffney.NumberTheory;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Coady Duffney
 */
public class LowestCommonDenominatorTest {
    
    public LowestCommonDenominatorTest() {
    }
    
    @Test
    public void test() {
        assertEquals(6, NumberTheory.lcm(3, 6));
        assertEquals(40, NumberTheory.lcm(10, 8));
        assertEquals(15, NumberTheory.lcm(3, 5));
    }
}
